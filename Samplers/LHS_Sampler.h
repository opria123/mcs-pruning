/*
 * LHS_Sampler.h
 *
 *  Created on: Sep 8, 2011
 *      Author: Rob
 */

#ifndef LHS_SAMPLER_H_
#define LHS_SAMPLER_H_

#include <vector>

#include "MCS_Sampler.h"
#include "../include/sampling.h"
#include "../include/mt.h"
#include "../include/phev.h"
#include "../include/logging.h"

class LHS_Sampler: public MCS_Sampler {
    public:
        LHS_Sampler();
        virtual ~LHS_Sampler();

        virtual void run(MTRand& mt);
        void setNumSamples(int ni);
        void setBatchSize(int ni);

        double getNumSamples();
        double getBatchSize();
        
    private:
        int numSamples, batchSize;
        std::vector < std::vector < double > > sampleMatrix;
        std::vector < std::vector < double > > linesMatrix;
};

#endif /* LHS_SAMPLER_H_ */
