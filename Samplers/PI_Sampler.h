/*
 * LHS_Sampler.h
 *
 *  Created on: Sep 8, 2011
 *      Author: Rob
 */

#ifndef PI_SAMPLER_H_
#define PI_SAMPLER_H_

#include <vector>

#include "MCS_Sampler.h"
#include "../include/sampling.h"
#include "../include/phev.h"
#include "../include/logging.h"
#include "../include/mt.h"
class PI_Sampler: public MCS_Sampler {
    public:
        PI_Sampler();
        virtual ~PI_Sampler();

        virtual void run(MTRand& mt);
};

#endif /* LHS_SAMPLER_H_ */
