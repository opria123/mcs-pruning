/*
 * PSO_Pruner.h
 *
 *  Created on: Feb 25, 2011
 *      Author: rgreen
 */

#ifndef CFO_SAMPLER_H_
#define CFO_SAMPLER_H_

#include "Sampler.h"
#include "../include/sampling.h"
#include "../include/mt.h"

class CFO_Sampler : public Sampler {
	public:
		CFO_Sampler(int _Np, int _Nd, int _Nt);
		virtual ~CFO_Sampler();
		virtual void run(MTRand& mt);
		bool checkConvergence(int j);

        virtual void Init(std::map < std::string, double > fs, std::map < std::string, double > ss, std::vector < Generator > g, double load, Classifier* l,
							int nb, int tv = 1539000, double p = 0.05, double r = 0.8);
		virtual void Init(std::map < std::string, double > fs, std::map < std::string, double > ss, std::vector < Generator > g, std::vector < Line > t, double load, Classifier* l,
					int nb, int tv = 1539000, double p = 0.05, double r = 0.8);

	protected:
        void    IPD();
        void    initA();
		void 	evaluateFitness(int j);
		void 	clearVectors();
        void    updatePositions(int j);
        void    updateAcceleration(int j);

		double sumX, sumXSquared;
		double nSumX, nSumXSquared;
		double sigma;
		long double epsilon;

		int Np, Nd, Nt;
		double gBestValue;
		double change, totalChange;

        bool useAccelClipping;

		int numProbesPerAxis, lastStep;
       	int bestTimeStep, bestProbeNumber;
	    int numEvals, numCorrections;

        double Alpha, Beta, Gamma;
       	double Frep, deltaFrep;
	    double bestFitness, diagLength;
	    double Denom, Numerator;
	    double SumSQ, DiagLength;
	    double aMax;
	    double positionTime,	correctionTime, 
	           fitnessTime,		accelTime, 
	           shrinkTime,		convergeTime,
               totalTime, 		clippingTime;
      
        std::vector <double> xMin, xMax;
	    std::vector < std::vector < std::vector < double > > > R, A;
		std::vector < std::vector < std::vector < double > > > binaryR;
	    std::vector < std::vector < double > > M;
	    std::vector < double >  bestFitnessArray;
	    std::vector < int >		bestProbeNumberArray;
};

#endif 
