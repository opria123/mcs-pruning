/*
 * DS_Sampler.h
 *
 *  Created on: Sep 8, 2011
 *      Author: Rob
 */

#ifndef DS_SAMPLER_H_
#define DS_SAMPLER_H_

#include <vector>

#include "MCS_Sampler.h"
#include "../include/sampling.h"
#include "../include/mt.h"
#include "../include/phev.h"gi

class DS_Sampler: public MCS_Sampler {
	public:
		DS_Sampler();
		virtual ~DS_Sampler();

		virtual void run(MTRand& mt);
		void setNumSamples(int ni);

		double getNumSamples();
		
	private:
		int numSamples;
		std::vector < std::vector < double > > samplingMatrix;
		std::vector < std::vector < double > > linesMatrix;
};

#endif /* LHS_SAMPLER_H_ */
