/*
 * PSO_Pruner.cpp
 *
 *  Created on: Feb 25, 2011
 *      Author: rgreen
 */

#include "CFO_Sampler.h"

#include <iostream>
#include <iomanip>
using namespace std;

CFO_Sampler::CFO_Sampler(int _Np, int _Nd, int _Nt) : Sampler() {

	Np = _Np;
	Nd = _Nd;
	Nt = _Nt;

	epsilon = pow(10.0,-7);
}

CFO_Sampler::~CFO_Sampler(){
    R.clear(); A.clear(); M.clear(); binaryR.clear();
	xMin.clear(); xMax.clear();
    bestProbeNumberArray.clear();
	bestFitnessArray.clear();
}

void CFO_Sampler::Init(std::map < std::string, double > fs, std::map < std::string, double > ss, std::vector < Generator > g, double load, Classifier* l,
							int nb, int tv, double p, double r){

    Sampler::Init(fs, ss, g, load, l, nb, tv, p, r);
    
    Alpha = 1; Beta = 3; 
    Frep = 0.5;
	deltaFrep = 0.4;
	useAccelClipping = false;
	
	#ifdef _MSC_VER
		bestFitness = -std::numeric_limits<double>::infinity();
	#else
		bestFitness = -INFINITY;
	#endif

    bestTimeStep     = 0; 
	bestProbeNumber	 = 0;
    numProbesPerAxis = Np/Nd == 0 ? 1 : Np/Nd;
    numEvals		 = 0;
	numCorrections   = 0;

	aMax = 1.0;
	positionTime= 0;	correctionTime	= 0;
	fitnessTime = 0,	accelTime		= 0; 
    shrinkTime  = 0;	convergeTime	= 0;
    totalTime	= 0; 	clippingTime	= 0;

    binaryR.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    R.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    A.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    M.resize(Np, std::vector < double > (Nt, 0));
	xMin.resize(Nd, 0); xMax.resize(Nd, 1);
    bestProbeNumberArray.resize(Nt,0);
	bestFitnessArray.resize(Nt,0);

}

void CFO_Sampler::Init(std::map < std::string, double > fs, std::map < std::string, double > ss, std::vector < Generator > g, std::vector < Line > t, double load, Classifier* l,
					int nb, int tv, double p, double r){

    Sampler::Init(fs, ss, g, t, load, l, nb, tv, p, r);

    Alpha = 1; Beta = 2; 
    Frep = 0.5;
	deltaFrep = 0.4;
	useAccelClipping = false;
	
	#ifdef _MSC_VER
		bestFitness = -std::numeric_limits<double>::infinity();
	#else
		bestFitness = -INFINITY;
	#endif

    bestTimeStep     = 0; 
	bestProbeNumber	 = 0;
    numProbesPerAxis = Np/Nd == 0 ? 1 : Np/Nd;
    numEvals		 = 0;
	numCorrections   = 0;

	aMax = 1.0;
	positionTime= 0;	correctionTime	= 0;
	fitnessTime = 0,	accelTime		= 0; 
    shrinkTime  = 0;	convergeTime	= 0;
    totalTime	= 0; 	clippingTime	= 0;

    binaryR.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    R.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    A.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    M.resize(Np, std::vector < double > (Nt, 0));
	xMin.resize(Nd, 0); xMax.resize(Nd, 1);
    bestProbeNumberArray.resize(Nt,0);
	bestFitnessArray.resize(Nt,0);
}
void CFO_Sampler::evaluateFitness(int j){
	bool failed;
	long double sol, stateProb;
	double totalCap, excess;
	std::string curSolution;
	std::vector<double> vCurSolution;

    int		totalUp 		= 0,	// Generators up in a single group
			totalDown 		= 0,	// Total Generators in a single group
			tGens 			= 1,
			tUp 			= 1,
			curBus			= 0,
			nextBus			= 0;
    double  copy = 1.0;
    if(useLines){ vCurSolution.resize(gens.size() + lines.size() + 2, 0);}
    else        { vCurSolution.resize(gens.size() + 2, 0);}


	for(int p=0; p<Np; p++){

		stateProb   = 1; totalCap = 0;
		curSolution = "";
		genCount    = 0; lineCount = 0;


        for(unsigned int i=0; i< gens.size(); i++){
		    nextBus = gens[i].getBus();
		    if(nextBus != curBus){
			    curBus = nextBus;
			    copy *= Mt::combination(tGens, tUp);
			    tUp = 0; tGens = 0;
		    }
		    tGens++;

            vCurSolution[i] = R[p][i][j];

		    if(vCurSolution[i] == 1){
			    stateProb 	*= (1-gens[i].getOutageRate());
			    totalCap += gens[i].getPG()/100;
			    curSolution += "1";
			    tUp++; totalUp++;
		    }else{
                curSolution += "0";
			    stateProb 	*=  gens[i].getOutageRate();
			    genCount++;
		    }
	    }
        copy *= stateProb;
        
		avgGenCount += genCount;
		genOutageCounts[genCount]++;

		if(useLines && lines.size() > 0){

			for(unsigned int i=0; i<lines.size(); i++){
                vCurSolution[i+gens.size()] = R[p][i+gens.size()][j];

				if(vCurSolution[i] == 1){
					vCurSolution[i+gens.size()] = 1;
					stateProb *= (1-lines[i].getOutageRate()*lineAdjustment);
					curSolution += "1";
				}else{
					curSolution += "0";
					vCurSolution[i+gens.size()] = 0;
					stateProb *= (lines[j].getOutageRate()*lineAdjustment);
					lineCount++;
				}
			}
			avgLineCount += lineCount;
			tLineOutageCounts[lineCount]++;
		}

        if(useLines){
		    vCurSolution[gens.size()+lines.size()] 	 = stateProb;
		    vCurSolution[gens.size()+lines.size()+1] = totalCap;
        }else{
            vCurSolution[gens.size()] 	= stateProb;
		    vCurSolution[gens.size()+1] = totalCap;
        }

		timer1.startTimer();
		if(successStates.find(curSolution) != successStates.end()){
			Collisions++;
			continue;
		}
		timer1.stopTimer();
		searchTime += timer1.getElapsedTime();

		classifier->init();
		sol = classifier->run(vCurSolution, excess);
		if(sol != 0.0){ failed = true;}
        else          { failed = false;}

		if(stateProb < epsilon){
			stateProb = epsilon;
			failed = true;
		}
		sampledStateProbs[curSolution] = stateProb;
        
        M[p][j] = stateProb;
        numEvals++;

        if(M[p][j] > bestFitness){
            bestFitness 	= M[p][j];
			bestTimeStep 	= j;
			bestProbeNumber = p;
			
			bestFitnessArray[j]		= M[p][j];
			bestProbeNumberArray[j] = p;
		}

		if(failed){
			sampledStates[curSolution] = 0;
            failedStates[curSolution]  = stateProb; 
			uniqueStates["1" + curSolution] = vCurSolution;
		}else{
			sampledStates[curSolution] = 1;
            successStates[curSolution]  = stateProb;
			uniqueStates["0" + curSolution] = vCurSolution;
		}
	}
}

bool CFO_Sampler::checkConvergence(int j){
	bool retValue = false;
	double totalProbDown = 0, totalProbUp=0;
    int count = 0;

	numSamples = sampledStates.size();
	std::map<string, int>::iterator i;
	
	for(i=sampledStates.begin(); i!= sampledStates.end(); i++){
		if(i->second == 0) {
			totalProbDown += sampledStateProbs[i->first];
			count++;
		}else{
			totalProbUp += sampledStateProbs[i->first];
		}
	}
  

	pLOLP   = LOLP;
	LOLP    = totalProbDown;
    vLOLP   = (1/numSamples) * (sumXSquared/numSamples - pow(LOLP,2));
    sigma   = sqrt((1.0/sampledStates.size()) * ((sampledStates.size()-count)/(double)sampledStates.size() - pow(LOLP,2)));
    sigmas.push_back(sigma);
	NLOLP   = totalProbUp;
	//LOLP    = (1-NLOLP+LOLP)/2.0;
    
	change += fabs(LOLP - pLOLP);

    //std::cout << totalProbDown << " " << LOLP << " " << NLOLP << " " << change/j << " " << sigma << " " << failedStates.size() << " " << successStates.size() << std::endl;
    std::cout << LOLP << " " << NLOLP << " " << LOLP/(LOLP+NLOLP) << " " << (NLOLP+LOLP)/2.0 << " " << sampledStates.size() << "\n";


    LOLP    = (NLOLP+LOLP)/2.0;
	if(sigma < tolerance){
		retValue = true;
	}
	fStatesCount.push_back(failedStates.size());
	sStatesCount.push_back(successStates.size());

	return retValue;
}

void CFO_Sampler::initA(){

	std::vector < std::vector < double > > tempR;
	MTRand mt;
    tempR= sampling::hammersleySampling(Nd, Np);
	for(int p=0; p<Np; p++){
		for(int i=0; i<Nd; i++){
			A[p][i][0] = 1-tempR[p][i];
		}
	}
	tempR.clear();
}

void CFO_Sampler::updatePositions(int j){
    timer.startTimer();
	Primes primes(j);
    double curPrime, k;

	for (int p = 0; p < Np; p++) {
		curPrime = primes.nextPrime();
		for (int i = 0; i < Nd; i++) {    
            //k = Utils::corputBase(curPrime, p*j*i);
            k = sampling::corputBase(curPrime, p*j*i);
            //k = Frep;
            if(A[p][i][j - 1] < k)  { R[p][i][j] = 0;}
            else					{ R[p][i][j] = 1;}
		}
	}
	timer.stopTimer();
	positionTime += timer.getElapsedTime();
}

void CFO_Sampler::IPD(){
    
    std::vector<double> dist(Np, 0);
    std::vector<double> meanDist(2, 0.0);
    std::vector<double> varDist(2, 0.0);

    for(int i=0; i<Nd; i++){
        R[0][i][0] = 0;
    }

    for(int p=1; p<Np; p++){
        for(int i=0; i<Nd; i++){

            // Try 0
            R[p][i][0] = 0;

            for(int k=0; k<p; k++){ // Every Other Particle
                dist[k] = 0;
                for(int l=0; l<Nd; l++){ // Every Other Dimension
                    if(R[p][l][0] != R[k][l][0]){ dist[k]++;}
                }
            }

            meanDist[0] = 0;
            for(int x=0; x<p; x++){meanDist[0] += dist[x]; }
            meanDist[0] = meanDist[0]/p;

            varDist[0] = 0;
            for(int x=0; x<p; x++){
                varDist[0] += (dist[x]-meanDist[0])*(dist[x]-meanDist[0]); 
            }

            // Try 1
            R[p][i][0] = 1;
            for(int k=0; k<p; k++){ // Every Other Particle
                dist[k] = 0;
                for(int l=0; l<Nd; l++){ // Every Other Dimension
                    if(R[p][l][0] != R[k][l][0]){ dist[k]++;}
                }
            }
            meanDist[1] = 0;
            for(int x=0; x<p; x++){meanDist[1] += dist[x]; }
            meanDist[1] = meanDist[1]/p;

            varDist[1] = 0;
            for(int x=0; x<p; x++){
                varDist[1] += (dist[x]-meanDist[1])*(dist[x]-meanDist[1]); 
            }

            if(meanDist[0] < meanDist[1]){ 
				R[p][i][0] = 1;
				continue;
			}
            if(meanDist[0] > meanDist[1]){ 
				R[p][i][0] = 0;
				continue;
			}  
            if(varDist[0] > varDist[1] ){ R[p][i][0] = 1;}
            else                        { R[p][i][0] = 0;}
        }
    }

    for(int i=0; i<Nd; i++){
        if(sampling::corputBase(2, i) < 0.5){
            
            if(sampling::corputBase(2, i+1) < 0.3) { R[0][i][0]    = 0;}
            else                                { R[0][i][0]    = 1;}
            if(sampling::corputBase(2, i+2) < 0.6) { R[1][i][0]    = 0;}
            else                                { R[0][i][0]    = 1;}
            if(sampling::corputBase(2, i+3) < 0.9) { R[Np-1][i][0] = 1;}
            else                                { R[0][i][0]    = 0;}

        }else{

            if(sampling::corputBase(2, i+1) < 0.3) { R[0][i][0]    = 1;}
            else                                { R[0][i][0]    = 0;}
            if(sampling::corputBase(2, i+2) < 0.6) { R[1][i][0]    = 1;}
            else                                { R[0][i][0]    = 0;}
            if(sampling::corputBase(2, i+3) < 0.9) { R[Np-1][i][0] = 0;}
            else                                { R[0][i][0]    = 1;}
        }
    }
   
}
void CFO_Sampler::updateAcceleration(int j){
    timer.startTimer();
    std::vector<double> maxA, minA;

     #ifdef _MSC_VER
		maxA.resize(Nd, -std::numeric_limits<double>::infinity());
        minA.resize(Nd, std::numeric_limits<double>::infinity());
	#else
        maxA.resize(Nd, -INFINITY);
        minA.resize(Nd, INFINITY);
	#endif
    
    // Update
	for (int p = 0; p < Np; p++) {
		for (int i = 0; i < Nd; i++) {
			A[p][i][j] = 0;

			for (int k = 0; k < Np; k++) {
				if (k != p) {
					SumSQ = 0.0;
                    Numerator = M[k][j] - M[p][j];

                    // Hamming Distance
					for (int L = 0; L < Nd; L++) {	
                        if(R[k][L][j] != R[p][L][j]){ SumSQ += 1;}
					}

					if(SumSQ != 0){
						Denom      = sqrt(SumSQ);    
						A[p][i][j] = A[p][i][j] + (R[k][i][j] - R[p][i][j]) * pow(Numerator,Alpha)/pow(Denom,Beta);
					}else{
                        A[p][i][j] = A[p][i][j] + Numerator;
                    }
				}
			}
            if(A[p][i][j] > maxA[i]){ maxA[i] = A[p][i][j];}
            if(A[p][i][j] < minA[i]){ minA[i] = A[p][i][j];}
		}
	}

    // Scale & Perturb Accel == 0
    Primes primes(j);
    for (int p = 0; p < Np; p++) {
        for(int i=0; i<Nd; i++) {

            if(fabs(minA[i]) != fabs(maxA[i])){
                A[p][i][j] =  (A[p][i][j]-minA[i])/(maxA[i]-minA[i]);
            }else if(fabs(minA[i]) == fabs(maxA[i]) || A[p][i][j] == 0 || A[p][i][j] == 1){
                
				if(sampling::corputBase(primes.nextPrime(), p*Nd+i) < 0.5){
                    A[p][i][j] += sampling::corputBase(primes.nextPrime(), p*Nd+i+1);
                }else{
                    A[p][i][j] += 1-sampling::corputBase(primes.nextPrime(), p*Nd+i+2);
                }

            }
        }
    }
	timer.stopTimer();
	accelTime += timer.getElapsedTime();
    maxA.clear();
}

void CFO_Sampler::run(MTRand& mt){
	bool converged = false;
	int j;

	stateGenerationTime = searchTime = 0;
	tLineOutageCounts.clear(); genOutageCounts.clear();
	tLineOutageCounts.resize(lines.size(), 0); genOutageCounts.resize(gens.size()+1, 0);
	
	timer.startTimer();

	sumX=0; sumXSquared=0; nSumX=0; nSumXSquared=0;
	curProb 	 = 0.0; vLOLP 		 = 0.0; pLOLP = 0.0;
	numSamples 	 = 1; 	iterations 	 = 0;	sigma = 1.0;
	LOLP 		 = 0.0; pLOLP = 0.0;
	Collisions	 = 0;	avgLineCount = 0;	avgGenCount  = 0;

	uniqueStates.clear(); sampledStateProbs.clear(); sampledStates.clear();

	j=1;
	IPD();                 
	initA();	 
    evaluateFitness(0);     // Init M

    change = 0.0;
	//while(!converged){
	while(j < Nt){
        std::cout << j << " "; 
        updatePositions(j);
		evaluateFitness(j);
        updateAcceleration(j);

        Frep += deltaFrep;
        if(Frep > 1){ Frep = 0.05; }

		converged = checkConvergence(j);
		converged = false;
       
		j++;
	}
	iterations = j;
    lastStep = j;

	timer.stopTimer();
	simulationTime = timer.getElapsedTime();
}

