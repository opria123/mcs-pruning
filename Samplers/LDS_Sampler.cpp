/*
 * LDS_Sampler.cpp
 *
 *  Created on: Sep 8, 2011
 *      Author: Rob
 */

#include "LDS_Sampler.h"

LDS_Sampler::LDS_Sampler() : MCS_Sampler(){
	// TODO Auto-generated constructor stub
	numSamples = 15000;
}
LDS_Sampler::LDS_Sampler(pruning::SAMPLING_METHOD st, int Ns) : MCS_Sampler(){
	numSamples	 = Ns;
	samplingType = st;
}
LDS_Sampler::LDS_Sampler(int Ns) : MCS_Sampler(){
	numSamples = Ns;
}
LDS_Sampler::LDS_Sampler(pruning::SAMPLING_METHOD st) : MCS_Sampler(){
	numSamples	 = 15000;
	samplingType = st;
}

LDS_Sampler::~LDS_Sampler() {
	// TODO Auto-generated destructor stub
}

void LDS_Sampler::run(MTRand& mt){
	double 	sumX,  	sumXSquared, nSumX, nSumXSquared,
			sol, 	excess, totalCap, sigma, nSigma, stateProb;
	bool 	failed;
	std::string curSolution;
	std::vector<double> curSample;
	std::vector<double> vCurSolution;
	std::vector<double> phevLoad, phevGen;
	int curBus	= 0, nextBus = 0, tUp, tGens;
	double copy, localNumSamples;
	curSolution = "";
	failed = false;
	vCurSolution.resize(gens.size() + lines.size() + 2, 0);
	sumX=0; sumXSquared=0; nSumX=0; nSumXSquared=0;
	curProb 	 = 0.0; vLOLP 		 = 0.0; pLOLP = 0.0;
	localNumSamples 	 = 1; 	iterations 	 = 0;	sigma = 1.0;
	nSigma		 = 1.0;	LOLP 		 = 0.0; pLOLP = 0.0;
	Collisions	 = 0;	avgLineCount = 0;	avgGenCount  = 0;

	stateGenerationTime = searchTime = 0;
	tLineOutageCounts.clear(); 			genOutageCounts.clear();
	uniqueStates.clear(); 				sampledStates.clear();
	uniqueFailedStatesCount.clear(); 	uniqueSuccessStatesCount.clear();
	fStatesCount.clear(); 				sStatesCount.clear();
	localFailedStates.clear(); 			localSuccessStates.clear();
	sampledStateOccurrences.clear();
	lolps.clear(); 	sigmas.clear(); vLolps.clear();
	nLolps.clear(); nSigmas.clear();

	tLineOutageCounts.resize(lines.size()+1, 0); genOutageCounts.resize(gens.size()+1, 0);

	timer.startTimer();
	sampleMatrix.clear();
	switch(samplingType){
		case pruning::SM_HAM:
			sampleMatrix = sampling::hammersleySampling(gens.size(), numSamples);
			if(useLines){ linesMatrix = sampling::hammersleySampling(lines.size(), numSamples);}
			break;
		case pruning::SM_FAU:
			sampleMatrix = sampling::faureSampling(gens.size(), numSamples);
			if(useLines){ linesMatrix = sampling::faureSampling(lines.size(), numSamples);}
			break;

        case pruning::SM_SOBOL:
            sampleMatrix = sampling::sobol_points(numSamples, gens.size());
			if(useLines){ linesMatrix = sampling::haltonSampling(lines.size(), numSamples);}
            break;

		case pruning::SM_HAL: default:
			sampleMatrix = sampling::haltonSampling(gens.size(), numSamples);
			if(useLines){ linesMatrix = sampling::haltonSampling(lines.size(), numSamples);}
			break;        
	}

	for(unsigned int i=0; i<sampleMatrix.size(); i++){
		classifier->reset();
		curSolution = "";
		totalCap = 0.00;

		fStatesCount.push_back(0); sStatesCount.push_back(0);

		timer1.startTimer();
		stateProb = 1; genCount = 0;
		curBus = 0; nextBus = 0; copy = 1;
		tUp = 0; tGens = 0;

		curSample = sampleMatrix[i];
		for(unsigned int x=0; x<gens.size(); x++){
			nextBus = gens[x].getBus();
			if(nextBus != curBus){
				curBus = nextBus;
				copy *= Mt::combination(tGens, tUp);
				tUp = 0; tGens = 0;
			}
			tGens++;

			curProb = curSample[x];
			if(curProb <=  gens[x].getOutageRate()){
				curSolution    += "0";
				vCurSolution[x] = 0;
				genCount       += 1;
				stateProb 	   *= gens[x].getOutageRate();
			}else{
				curSolution     += "1";
				vCurSolution[x]  = 1;
				stateProb 		*= (1-gens[x].getOutageRate());
				totalCap 		+= gens[x].getPG()/100;
				tUp++;
			}
		}

		avgGenCount += genCount;
		genOutageCounts[genCount]++;

		lineCount = 0;
		if(useLines && lines.size() > 0){
			curSample = linesMatrix[i];
			for(unsigned int x=0; x<lines.size(); x++){
				curProb = curSample[x];
				if(curProb <= (lines[x].getOutageRate()*lineAdjustment)){
					curSolution += "0";
					vCurSolution[x+gens.size()] = 0;
					stateProb *= (lines[x].getOutageRate()*lineAdjustment);
					lineCount++;
				}else{
					curSolution += "1";
					vCurSolution[x+gens.size()] = 1;
					stateProb *= (1-lines[x].getOutageRate()*lineAdjustment);
				}
			}
			avgLineCount += lineCount;
			tLineOutageCounts[lineCount]++;
		}
		sampledStateProbs[curSolution] = stateProb;

		if(usePHEVs){
			Phev::calculatePHEVLoad(penetrationLevel, rho, totalVehicles, numBuses, phevLoad, phevGen, mt, phevPlacement);
			classifier->addLoad(phevLoad);
		}
		vCurSolution[gens.size()+lines.size()] 	 = stateProb;
		vCurSolution[gens.size()+lines.size()+1] = totalCap;


		timer1.stopTimer();
		stateGenerationTime += timer1.getElapsedTime();

		timer1.startTimer();
		if(successStates.find(curSolution) != successStates.end()){
			Collisions++;
			timer1.stopTimer();
			searchTime += timer1.getElapsedTime();
			continue;
		}
		timer1.stopTimer();
		searchTime += timer1.getElapsedTime();

		sol = classifier->run(vCurSolution,excess);

		if(sampledStateOccurrences.find(curSolution) == sampledStateOccurrences.end()){
			sampledStateOccurrences[curSolution] = 1;
		}else{
			sampledStateOccurrences[curSolution]++;
		}
		if(sol != 0){ failed = true;}
		else		{ failed = false;}

		if(failed){
			sumX++;	sumXSquared++;
			sampledStates.insert(std::pair<std::string, int>(curSolution, 1));
			uniqueStates["1" + curSolution] = vCurSolution;
			localFailedStates[curSolution] = stateProb;
			fStatesCount[iterations]++;
		}else{
			nSumX++; nSumXSquared++;
			sampledStates.insert(std::pair<std::string, int>(curSolution, 0));
			uniqueStates["0" + curSolution] = vCurSolution;
			localSuccessStates[curSolution] = stateProb;
			sStatesCount[iterations]++;
		}
		uniqueFailedStatesCount.push_back(localFailedStates.size());
		uniqueSuccessStatesCount.push_back(localSuccessStates.size());

		localNumSamples++;
		iterations++;

	/****************************************** Method 1 ******************************************/
		pLOLP = LOLP;
		LOLP = sumX/localNumSamples;
		vLOLP = (1/localNumSamples) * (sumXSquared/localNumSamples - pow(LOLP,2));
		sigma = sqrt(vLOLP)/LOLP;

		pNLOLP = NLOLP;
		NLOLP = nSumX/localNumSamples;
		vNLOLP = (1/localNumSamples) * (nSumXSquared/localNumSamples - pow(NLOLP,2));
		nSigma = sqrt(vNLOLP)/NLOLP;

		vLolps.push_back(vLOLP);
		lolps .push_back(LOLP);
		sigmas.push_back(sigma);

		nLolps .push_back(NLOLP);
		nSigmas.push_back(nSigma);

		//if(sigma < tolerance || nSigma < tolerance) { break;}
        if(sigma < tolerance) { break;}
	}

	ofstream myFile;
	string fileName = "LDS_Convergence_" + curSystem + "_" + Utils::getSamplingMethodString(samplingType) + "_";
	fileName = sampling::changeBase(fileName, numSamples);
	fileName = fileName + ".csv";
	myFile.open(fileName.c_str());
	if(myFile.is_open()){
		for(unsigned int i=0; i<lolps.size(); i++){
			myFile	<< i+1		<< "\t" 
					<< lolps[i] << "\t"
					<< vLolps[i] << "\t"
					<< sigmas[i]<< std::endl;
		}
	}
	myFile.close();

	avgLineCount /= localNumSamples;
	avgGenCount  /= localNumSamples;

	timer.stopTimer();
	simulationTime = timer.getElapsedTime();

	vCurSolution.clear();
	phevLoad.clear();
	phevGen.clear();
}

void LDS_Sampler::setNumSamples(int ni)	{ numSamples = ni;}

double LDS_Sampler::getNumSamples()		{ return numSamples;}
