/*
 * CFOPruner.cpp
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#include <map>
#include <vector>
#include "CFOPruner.h"
#include <iostream>

#include <limits>

CFOPruner::CFOPruner(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
         : Pruner(popSize, generations, g, l, o, p, ul){

    Init(popSize, generations, o, g, l, p, ul);	 
}

void CFOPruner::Init(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul){

    Pruner::Init(popSize, generations, g, l, o, p, ul);

    Alpha = 1; Beta = 2; 
    Frep = 0.5;
    deltaFrep = 0.4;
    useAccelClipping = false;
    
    #ifdef _MSC_VER
        bestFitness  = -std::numeric_limits<double>::infinity();
        worstFitness = std::numeric_limits<double>::infinity();
    #else
        bestFitness  = -INFINITY;
        worstFitness = INFINITY;
    #endif

    bestTimeStep     = 0; 
    bestProbeNumber	 = 0;
    worstTimeStep    = 0;
    worstProbeNumber = 0;
    numProbesPerAxis = Np/Nd == 0 ? 1 : Np/Nd;
    numEvals		 = 0;
    numCorrections   = 0;

    aMax = 1.0;
    positionTime= 0;	correctionTime	= 0;
    fitnessTime = 0,	accelTime		= 0; 
    shrinkTime = 0;	    convergeTime	= 0;
    totalTime	= 0; 	clippingTime	= 0;

    binaryR.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    R.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    A.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    M.resize(Np, std::vector < double > (Nt, 0));
    xMin.resize(Nd, 0); xMax.resize(Nd, 1);
    bestProbeNumberArray.resize(Nt,0);
    bestFitnessArray.resize(Nt,0);
    diversity.resize(Nt, 0);
}

void CFOPruner::Reset(int np, int nt){
    Pruner::Reset(np, nt);
    clearTimes();
    clearVectors();
    
    bestTimeStep     = 0; 
    bestProbeNumber	 = 0;
    worstTimeStep    = 0;
    worstProbeNumber = 0;
    numProbesPerAxis = Np/Nd;
    numEvals		 = 0;
    numCorrections   = 0;
    
    #ifdef _MSC_VER
        bestFitness  = -std::numeric_limits<double>::infinity();
        worstFitness = std::numeric_limits<double>::infinity();
    #else
        bestFitness  = -INFINITY;
        worstFitness = INFINITY;
    #endif

    positionTime = 0, correctionTime = 0, 
    fitnessTime  = 0, accelTime      = 0, 
    shrinkTime   = 0, convergeTime   = 0,
    totalTime    = 0;
    
    R.clear(); A.clear(); M.clear(); binaryR.clear();
    xMin.clear(); xMax.clear();
    bestProbeNumberArray.clear();
    bestFitnessArray.clear();
    
    binaryR.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    R.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    A.resize(Np, std::vector < std::vector < double > >(Nd, std::vector < double >(Nt, 0)));
    M.resize(Np, std::vector < double > (Nt, 0));
    xMin.resize(Nd, 0); xMax.resize(Nd, 1);
    bestProbeNumberArray.resize(Nt,0);
    bestFitnessArray.resize(Nt,0);
    diversity.resize(Nt, 0);
}

void CFOPruner::clearVectors(){
    Pruner::clearVectors();
    deltaFrep = 0.1;
    R.clear(); A.clear(); M.clear(); binaryR.clear();
    diversity.clear();
}
CFOPruner::~CFOPruner() {
    R.clear(); A.clear(); M.clear(); binaryR.clear();
    xMin.clear(); xMax.clear();
    bestProbeNumberArray.clear();
    bestFitnessArray.clear();
    diversity.clear();
}
void CFOPruner::IPD(){
    
    timer.startTimer();
    Primes primes;
    double curPrime, k;

    for (int p = 0; p < Np; p++) {
        curPrime = primes.nextPrime();
        for (int i = 0; i < Nd; i++) {    
            k = 1-sampling::corputBase(curPrime, i);
            if(Mt::sigMoid(A[p][i][0]) < k)  { R[p][i][0] = 0;}
            else					            { R[p][i][0] = 1;}
        }
    }
    timer.stopTimer();
    positionTime += timer.getElapsedTime();
    /*
    std::vector<double> dist(Np, 0);
    std::vector<double> meanDist(2, 0.0);
    std::vector<double> varDist(2, 0.0);

    for(int i=0; i<Nd; i++){
        R[0][i][0] = 0;
    }

    for(int p=1; p<Np; p++){
        for(int i=0; i<Nd; i++){

            // Try 0
            R[p][i][0] = 0;

            for(int k=0; k<p; k++){ // Every Other Particle
                dist[k] = 0;
                for(int l=0; l<Nd; l++){ // Every Other Dimension
                    if(R[p][l][0] != R[k][l][0]){ dist[k]++;}
                }
            }

            meanDist[0] = 0;
            for(int x=0; x<p; x++){meanDist[0] += dist[x]; }
            meanDist[0] = meanDist[0]/p;

            varDist[0] = 0;
            for(int x=0; x<p; x++){
                varDist[0] += (dist[x]-meanDist[0])*(dist[x]-meanDist[0]); 
            }


            // Try 1
            R[p][i][0] = 1;
            for(int k=0; k<p; k++){ // Every Other Particle
                dist[k] = 0;
                for(int l=0; l<Nd; l++){ // Every Other Dimension
                    if(R[p][l][0] != R[k][l][0]){ dist[k]++;}
                }
            }
            meanDist[1] = 0;
            for(int x=0; x<p; x++){meanDist[1] += dist[x]; }
            meanDist[1] = meanDist[1]/p;

            varDist[1] = 0;
            for(int x=0; x<p; x++){
                varDist[1] += (dist[x]-meanDist[1])*(dist[x]-meanDist[1]); 
            }

            if(meanDist[0] < meanDist[1]){ 
                R[p][i][0] = 1;
                continue;
            }
            if(meanDist[0] > meanDist[1]){ 
                R[p][i][0] = 0;
                continue;
            }  
            if(varDist[0] > varDist[1] ){ R[p][i][0] = 1;}
            else                        { R[p][i][0] = 0;}
        }
    }
   */
}
void CFOPruner::calculateDiagLength(){

    diagLength = 0;
    for (int i = 0; i < Nd; i++) {
        diagLength += pow(xMax[i] - xMin[i], 2);
    }
    diagLength = sqrt(diagLength);
}
void CFOPruner::initA(){

    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = sampling::haltonSampling(Nd, Np); 
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            A[p][i][0] = 1-tempR[p][i];
        }
    }
    tempR.clear();
}
void CFOPruner::evaluateFitness(int j){
    timer.startTimer();
    std::vector<int> pos(Nd, 0);

    for (int p = 0; p < Np; p++) {
        for(int i=0; i<Nd; i++){
            pos[i] = (int)R[p][i][j];
        }
        M[p][j] = EvaluateSolution(pos);
        numEvals++;

        if(M[p][j] >= bestFitness){
            bestFitness 	        = M[p][j];
            bestTimeStep 	        = j;
            bestProbeNumber         = p;
            
            bestFitnessArray[j]		= M[p][j];
            bestProbeNumberArray[j] = p;
        }
         if(M[p][j] <= worstFitness){
            worstFitness 	        = M[p][j];
            worstTimeStep 	        = j;
            worstProbeNumber        = p;
        }
    }
    timer.stopTimer();
    fitnessTime += timer.getElapsedTime();

    pos.clear();
}
void CFOPruner::mutate(int j){
    std::vector < std::vector < double > > tempR;

    tempR = sampling::haltonSampling(Nd, Np); 
    for(int p=0; p<Np;p++){
        for(unsigned int i=0; i<gens.size(); i++){
            if(tempR[p][i] < gens[i].getOutageRate()) { R[p][i][j] = 0;}
            else                                    { R[p][i][j] = 1;}
        }
        if(useLines && lines.size() > 0){
            for(unsigned int i=0; i<lines.size(); i++){
                if(tempR[p][gens.size() + i] < lines[i].getOutageRate()) { R[p][gens.size() + i][j] = 0;}
                else                                                     { R[p][gens.size() + i][j] = 1;}
            }
        }
    }
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            A[p][i][totalIterations] *= (1-Frep);
        }
    }
}

void CFOPruner::updatePositions(int j){
    timer.startTimer();
    Primes primes(j);
    double curPrime, k;

    for (int p = 0; p < Np; p++) {
        curPrime = primes.nextPrime();
        for (int i = 0; i < Nd; i++) {    
            k = 1 - sampling::corputBase(curPrime, p*j+i); // 28
            if(Mt::sigMoid(A[p][i][j-1]) < k) { R[p][i][j] = 0;}
            else					             { R[p][i][j] = 1;}
        }
    }
    timer.stopTimer();
    positionTime += timer.getElapsedTime();
}
void CFOPruner::updateAcceleration(int j){
    timer.startTimer();
    timer.startTimer();
    std::vector<double> maxA, minA;

     #ifdef _MSC_VER
        maxA.resize(Nd, -std::numeric_limits<double>::infinity());
        minA.resize(Nd, std::numeric_limits<double>::infinity());
    #else
        maxA.resize(Nd, -INFINITY);
        minA.resize(Nd, INFINITY);
    #endif
    
    // Update
    for (int p = 0; p < Np; p++) {
        for (int i = 0; i < Nd; i++) {
            A[p][i][j] = 0;
                        
            for (int k = 0; k < Np; k++) {
                if (k != p) {
                    SumSQ = 0.0;
                    for (int L = 0; L < Nd; L++) {	
                        SumSQ = SumSQ + pow(R[k][L][j] - R[p][L][j], 2);
                    }

                    if(SumSQ != 0){
                        Numerator = Mt::unitStep(M[k][j] - M[p][j]) * (M[k][j]- M[p][j]);
                        Denom      = sqrt(SumSQ);    
                        A[p][i][j] = A[p][i][j] + (R[k][i][j] - R[p][i][j]) * pow(Numerator,Alpha)/pow(Denom,Beta);
                    }
                }
            }
            
            if(A[p][i][j] > maxA[i]){ maxA[i] = A[p][i][j];}
            if(A[p][i][j] < minA[i]){ minA[i] = A[p][i][j];}
        }
    }

    for (int p = 0; p < Np; p++) {
        for(int i=0; i<Nd; i++) {
            if(fabs(minA[i]) != fabs(maxA[i])){
                A[p][i][j] =  (A[p][i][j]-minA[i])/(maxA[i]-minA[i]);
            }
        }
    }

    maxA.clear(); minA.clear();
}
void CFOPruner::clipAcceleration(int j){

    double aLength;
    timer.startTimer();

    calculateDiagLength();
    
    for(int p=0; p<Np; p++){
        aLength = 0;
        for(int i=0; i<Nd; i++){
            aLength += pow(A[p][i][j],2);
        }
        aLength = sqrt(aLength);
        if(aLength > aMax * diagLength){
            for(int i=0; i<Nd; i++){
                A[p][i][j] *= aMax;
            }
        }
    }
    timer.stopTimer();
    clippingTime += timer.getElapsedTime();
}

void CFOPruner::calculateDiversity(int j){
    double sum;

    sum = 0;
    for(int p=0; p<Np-1; p++){
        for(int l=p+1; l<Np; l++){
            for(int i=0; i<Nd; i++){
                sum += abs(R[p][i][j]-R[l][i][j]);
            }
        }
    }

    sum = sum * 2.0;
    sum = sum /(Np*(Np-1)*Nd);
    diversity[j] = sum;
}
void CFOPruner::Prune(MTRand& mt){

    timer.startTimer();
    IPD();                  // Init R
    initA();	            // Init A
    evaluateFitness(0);     // Init M
    lastStep = Nt;
    
    totalIterations = 1;

    aMax = 1.1;
    while(!isConverged()){      
        updatePositions(totalIterations);
        evaluateFitness(totalIterations);
        updateAcceleration(totalIterations);
     
        if(useAccelClipping){ clipAcceleration(totalIterations);}
    
        if(isConverged()){
            lastStep = totalIterations;
            break;
        }
        Frep += deltaFrep;
        if(Frep > 1){ Frep = 0.05; }
        
        calculateDiversity(totalIterations);
        if(diversity[totalIterations] < 0.3){
            mutate(totalIterations);
            evaluateFitness(totalIterations);
            updateAcceleration(totalIterations);
        }
        
        //cout << diversity[totalIterations] << " " << successStates.size() << " "  << collisions << "\n";
        totalIterations++;
    }//End Time Steps
    timer.stopTimer();
    pruningTime = timer.getElapsedTime();
}

bool CFOPruner::isConverged() {
    bool retValue = Pruner::isConverged();
    double totalHammingDist = 0, localHammingDist = 0;
    double avgFitness = 0, stdDev = 0;

    if(useLogging && totalIterations > 1){
        for(int p=0; p<Np; p++){
            avgFitness += M[p][totalIterations];
        }
        avgFitness /= ((double)Np);

        for(int p=0; p<Np; p++){
            stdDev += pow(M[p][totalIterations] - avgFitness,2);
        }
        stdDev /= (Np-1.0);
        stdDev = sqrt(stdDev);
        stdDevFitness.push_back(stdDev);

        totalHammingDist = 0;
        for(int p=0; p<Np; p++){
            for(int j=0; j<Np; j++){
                if(p != j){
                    localHammingDist = 0;
                    for(int i=0; i<Nd; i++){
                        localHammingDist +=  abs(R[p][i][totalIterations] - R[j][i][totalIterations]);
                    }
                    totalHammingDist += localHammingDist;
                }
            }
        }
        hammingDiversity.push_back(totalHammingDist/Np);
    }
    return retValue;
}