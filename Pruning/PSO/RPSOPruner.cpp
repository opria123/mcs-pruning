/*
 * RPSOPruner.cpp
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#include "RPSOPruner.h"
#include <iostream>
using namespace std;
/*
 * PSOPruner.cpp
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#include <map>
#include <vector>
#include "RPSOPruner.h"
#include "Utils.h"


RPSOPruner::RPSOPruner(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
         : Pruner(popSize, generations, g, l, o, p, ul){
    Init(popSize, generations, -3.05101, 1.70618, 2.0176, o, g, l, p, ul);
}

RPSOPruner::RPSOPruner(int popSize, int generations, double c1, double c2, double w, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
         : Pruner(popSize, generations, g, l, o, p, ul){
    Init(popSize, generations, c1, c2, w, o, g, l, p, ul);
}

void RPSOPruner::Init(int popSize, int generations, double c1, double c2, double w, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul){
    Pruner::Init(popSize, generations, g, l, o, p, ul);
    C1					= c1;
    C2					= c2;
    W					= w;

    A = 5; B = -3; C = 5;
}

void RPSOPruner::Reset(int np, int nt){
    Pruner::Reset(np, nt);
    clearTimes();
    clearVectors();
}


void RPSOPruner::clearVectors(){
    Pruner::clearVectors();
    swarm.clear();

}
RPSOPruner::~RPSOPruner() {
    // TODO Auto-generated destructor stub
}
void RPSOPruner::initPopulation(MTRand& mt){
    for(int i=0; i<Np; i++){
        binaryParticle p;

        for(int j=0; j<(int)gens.size(); j++){
            p.pos.push_back((mt.rand() < gens[j].getOutageRate() ? 0 : 1 ));
            p.vel.push_back(gens[j].getOutageRate());
            p.pBest.push_back(0);
        }
        if(useLines){
            for(int j=0; j<(int)lines.size(); j++){
                p.pos.push_back((mt.rand() < lines[j].getOutageRate() ? 0 : 1 ));
                p.vel.push_back(lines[j].getOutageRate());
                p.pBest.push_back(0);
            }
        }

        #ifdef _MSC_VER
            p.pBestValue = -std::numeric_limits<double>::infinity();
        #else
            p.pBestValue = -INFINITY;
        #endif
        swarm.push_back(p);
    }
}
void RPSOPruner::evaluateFitness(){
    double result;

    for(unsigned int i=0; i<swarm.size(); i++){
        result = EvaluateSolution(swarm[i].pos);

        //Local Best
        if(result > swarm[i].pBestValue){
            swarm[i].pBest      = swarm[i].pos;
            swarm[i].pBestValue = result;
        }
        //Global Best
        if(result > gBestValue){
            gBest       = swarm[i].pos;
            gBestValue  = result;
        }
    }
}

void RPSOPruner::Prune(MTRand& mt){

    timer.startTimer();
    gBest.resize(gens.size(), 0);
    
    #ifdef _MSC_VER
        gBestValue = -std::numeric_limits<double>::infinity();
    #else
        gBestValue = -INFINITY;
    #endif

    initPopulation(mt);
    totalIterations = 0;
    while(!isConverged()){
        evaluateFitness();
        updatePositions(mt);
        totalIterations++;
    }
    timer.stopTimer();
    pruningTime = timer.getElapsedTime();
}

double RPSOPruner::sigMoid(double v){
    return 1/(1+exp(-v));
}

void RPSOPruner::updatePositions(MTRand& mt){
    double R1, R2, R3, newV;

    for(unsigned int i=0; i< swarm.size(); i++){
        for(int j=0; j<Nd; j++){
            R1 = mt.rand();
            R2 = mt.rand();
            R3 = mt.rand();

            newV =  W * swarm[i].vel[j];
            newV += A * R1 * (-swarm[i].pos[j] + swarm[i].pBest[j]);
            newV += B * R2 * (-swarm[i].pos[j] + swarm[mt.randInt(swarm.size()-1)].pBest[j]);
            newV += C * R3 * W * swarm[mt.randInt(swarm.size()-1)].vel[j];
            newV = sigMoid(newV);
            swarm[i].vel[j] = newV;

            if(newV > mt.rand()){
                swarm[i].pos[j] = 1;
            }else{
                swarm[i].pos[j] = 0;
            }

        }
    }
}
bool RPSOPruner::isConverged() {
    bool retValue = Pruner::isConverged();
    double totalHammingDist = 0, localHammingDist = 0;
    double avgFitness = 0, stdDev = 0;

    if(useLogging && totalIterations > 1){
        for(unsigned int i=0; i<swarm.size(); i++){
            avgFitness += swarm[i].fitness;
        }
        avgFitness /= ((double)swarm.size());

        for(unsigned int i=0; i<swarm.size(); i++){
            stdDev += pow(swarm[i].fitness - avgFitness,2);
        }
        stdDev /= (swarm.size()-1.0);
        stdDev = sqrt(stdDev);
        stdDevFitness.push_back(stdDev);

        totalHammingDist = 0;
        for(unsigned int i=0; i<swarm.size(); i++){
            for(unsigned int j=0; j<swarm.size(); j++){
                if(i != j){
                    localHammingDist = 0;
                    for(unsigned int k=0; k<swarm[i].pos.size(); k++){
                        localHammingDist +=  abs(swarm[i].pos[k] - swarm[i].pos[j]);
                    }
                    totalHammingDist += localHammingDist;
                }
            }
        }
        hammingDiversity.push_back(totalHammingDist/swarm.size());
    }

    return retValue;
}
