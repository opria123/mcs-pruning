#include "syst.h"
void Syst::loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem,
	std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& buses) {
	//Clear Vectors
	gens.clear(); lines.clear(); buses.clear();

	std::ifstream myFile;
	int     count = 0,
		numGens = 0;
	myFile.open(("../Data/" + curSystem).c_str());

	if (myFile.is_open()) {
		myFile >> numGens;
		myFile >> pLoad;
		myFile >> qLoad;
		myFile >> nb;
		myFile >> nt;

		std::vector<std::string> tokens;
		std::string s;

		// Generators
		for (int i = 0; i<numGens; i++) {
			myFile >> s;
			St::tokenizeString(s, tokens, ",");

			gens.push_back(Generator(
				atof(tokens[3].c_str()),
				atof(tokens[4].c_str()),
				atof(tokens[5].c_str()),
				atof(tokens[6].c_str()),
				atof(tokens[7].c_str()),
				atoi(tokens[0].c_str()))
			);

			gens[i].setIndex(count);
			count++;
			tokens.clear();
		}

		// Transmission Lines
		for (int i = 0; i<nt; i++) {
			myFile >> s;
			St::tokenizeString(s, tokens, ",");

			lines.push_back(Line(
				atoi(tokens[0].c_str()),
				atoi(tokens[1].c_str()),
				atoi(tokens[2].c_str()),
				atof(tokens[3].c_str()),
				atof(tokens[4].c_str()),
				atof(tokens[5].c_str()),
				atof(tokens[6].c_str()),
				atof(tokens[7].c_str()),
				atof(tokens[8].c_str()),
				atof(tokens[9].c_str()),
				atof(tokens[10].c_str()),
				atof(tokens[11].c_str()),
				atof(tokens[12].c_str()),
				atof(tokens[13].c_str()))
			);
			tokens.clear();
		}

		for (int i = 0; i<nb; i++) {
			myFile >> s;
			St::tokenizeString(s, tokens, ",");
			//id, type, area, zone, Pd, Qd, Gs, Bs, Vm, Va, vMin, vMax, baseKva

			buses.push_back(Bus(
				atoi(tokens[0].c_str()),
				atoi(tokens[1].c_str()),
				atoi(tokens[6].c_str()),
				atoi(tokens[10].c_str()),
				atof(tokens[2].c_str()),
				atof(tokens[3].c_str()),
				atof(tokens[4].c_str()),
				atof(tokens[5].c_str()),
				atof(tokens[6].c_str()),
				atof(tokens[7].c_str()),
				atof(tokens[12].c_str()),
				atof(tokens[11].c_str()),
				atof(tokens[9].c_str()))
			);
			tokens.clear();
		}

		myFile.close();
	}
}
