#ifndef PHEV_H_
#define PHEV_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>


#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"
#include "sampling.h"
#include "st.h"
#include "mt.h"
namespace Phev
{
	extern void calculatePHEVLoad(
		double penetrationLevel, double rho,
		int totalVehicles, int numBuses,
		std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT PHEV_PLACEMENT = pruning::PP_EVEN_ALL_BUSES);

};
#endif