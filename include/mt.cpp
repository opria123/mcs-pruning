#include "Mt.h"

double Mt::sigMoid(double v) {
	return 1 / (1 + exp(-v));
}


int Mt::factorial(int n) {
	int result = 1;

	for (int i = 1; i <= n; ++i) {
		result = result*i;
	}

	return result;
}

// n Choose r
int Mt::combination(int n, int r) {
	return Mt::factorial(n) / (Mt::factorial(r) * Mt::factorial(n - r));
}
void Mt::twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A) {

	std::vector < std::vector < double > > L(A.size(), std::vector < double >(A.size(), 0));
	L[0][0] = sqrt(A[0][0]);
	L[0][1] = 0;
	L[1][0] = A[0][1] / L[0][0];
	L[1][1] = sqrt(A[1][1] - (L[1][0] * L[1][0]));
	A = L;
}

std::vector< std::vector<int> > Mt::matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B) {
	std::vector< std::vector<int> > C(A.size(), std::vector<int>(B.size(), 0));

	int sum = 0;

	for (int i = 0; i<(int)A.size(); i++) {
		for (int j = 0; j<(int)B[0].size(); j++) {
			sum = 0;
			for (int k = 0; k<(int)A[0].size(); k++) {
				sum = sum + A[i][k] * B[k][j];
			}
			C[i][j] = sum;
		}
	}
	return C;
}

std::vector<double> Mt::decToBin(double n, int numDigits) {
	std::vector<double> retVal;


	if (n <= 1) {
		retVal.push_back(n);
	}
	else {
		while (n > 0) {
			retVal.push_back(floor(fmod(n, 2)));
			//retVal.push_back((int)n % 2);
			n = floor(n / 2);
		}
	}

	while ((int)retVal.size() < numDigits) {
		retVal.push_back(0);
	}
	reverse(retVal.begin(), retVal.end());
	return retVal;
}
double Mt::unitStep(double X) {
	double retValue;

	if (X < 0.0) { retValue = 0.0; }
	else { retValue = 1.0; }

	return retValue;
}